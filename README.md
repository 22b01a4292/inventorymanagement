Welcome to Inventory Management System (IMS), an application designed to help manage employee details effectively. IMS allows you to add, update, delete, and search for employee information effortlessly.

INTRODUCTION

Inventory Management System is a Python-based application built using the Tkinter library for the graphical user interface and SQLite for database management. It provides a user-friendly interface for managing employee details including their ID, name, contact information, and more.

FEATURES

Add Employee: Easily add new employee details to the database.
Update Employee: Update existing employee information as needed.
Delete Employee: Remove employee records from the database.
Search Employee: Search for specific employees by various criteria such as name, email, or contact information.

DEPENDENCIES

To run the Inventory Management System, you need the following dependencies:

Python 3.6 or higher
Pillow 10.2.0
Tkinter 0.1.0
DB Browser for SQLite 3.12.2

INSTALLATION

Follow these steps to set up the Inventory Management System locally:

Clone the repository:

bash

git clone https://gitlab.com/22b01a4271/project.git

Navigate to the project directory:

bash

cd project

Install the required dependencies:

python -m pip install -r requirements.txt

(Optional) Download and install DB Browser for SQLite from the official website.

Getting Started

To run the Inventory Management System, execute the following command:

css

python main.py

Usage

1.Add Employee: Fill in the employee details in the provided fields and click the "Save" button to add the employee to the database.
2.Update Employee: Select an employee from the list, update their details in the respective fields, and click the "Update" button to save the changes.
3.Delete Employee: Select an employee from the list and click the "Delete" button to remove them from the database.
4.Search Employee: Choose a search criteria from the dropdown menu, enter the search term, and click the "Search" button to find matching employees.
